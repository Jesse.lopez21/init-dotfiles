# .bash_profile

# Get the aliases and functions
[ -f $HOME/.bashrc ] && . $HOME/.bashrc

(keybase service &> /dev/null & KEYBASE_RUN_MODE=prod kbfsfuse ~/kbfs &> /dev/null &)

# auto start X on tty1
# [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
[[ -t 0 && $(tty) == /dev/tty1 && ! $DISPLAY ]] && exec startx


