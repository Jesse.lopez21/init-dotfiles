#!/bin/bash

ps aux | grep i3lock | grep -v xautolock | grep -v grep

if [ $? -eq 1 ]
then
	notify-send -t 18000 -a xautolock "xautolock" "Locking screen in less than 20 seconds..."
fi
