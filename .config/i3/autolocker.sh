#!/bin/bash
lock=$HOME/.config/i3lock/i3autolocker.sh
alert=$HOME/.config/i3/lockalert.sh

notify-send -a xautolock "xautolock" "Started..."
xautolock -time 2 -locker $lock -detectsleep -nowlocker $lock -notify 20 -notifier $alert -secure
